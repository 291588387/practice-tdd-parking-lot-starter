# ORID
## O:
- In code review part, I understood how to implement strategy pattern in our homework.
- Practiced combination of OOP and TDD through the parking lot case, enhanced my comprehension of both concepts.
- Integrated the parking lot case story by story,  realized how an agile developing process will be in actual scenes.
## R:

- Fulfilled
## I:

- Combination of TDD and OOP is abundant.
- During practice I solidated my coding ability by connecting TDD and OOP.
## D:

- Keep on exploring TDD and OOP to strengthen proficiency