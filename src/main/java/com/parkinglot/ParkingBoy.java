package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoy {
    List<ParkingLot> parkingLots = new ArrayList<>();

    public void addParkingLot(ParkingLot parkingLot) {
        parkingLots.add(parkingLot);
    }

    public ParkingTicket parkCar(Car car) {
        return this.parkingLots.stream()
                .filter((parkingLot) -> !parkingLot.isFull())
                .findFirst()
                .map(lot->lot.park(car)).orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetch(ParkingTicket parkingTicket) {
        for (ParkingLot parkingLot : this.parkingLots) {
            if (parkingLot.isContains(parkingTicket)) return parkingLot.fetch(parkingTicket);
        }
        throw new UnrecognizedParkingTicketException();
    }
}
