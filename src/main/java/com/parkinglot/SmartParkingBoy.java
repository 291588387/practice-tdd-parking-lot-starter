package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class SmartParkingBoy extends ParkingBoy{

    @Override
    public ParkingTicket parkCar(Car car) {
        return this.parkingLots.stream()
                .filter((parkingLot) -> !parkingLot.isFull())
                .sorted((current, next) -> next.getSpareSpace() - current.getSpareSpace())
                .findFirst()
                .map(lot->lot.park(car))
                .orElseThrow(NoAvailablePositionException::new);

    }
}
