package com.parkinglot;

public class SuperSmartParkingBoy extends ParkingBoy{
    @Override
    public ParkingTicket parkCar(Car car) {
        final int maximum = Integer.MAX_VALUE;
        return this.parkingLots.stream()
                .filter((parkingLot) -> !parkingLot.isFull())
                .sorted((current, next) ->
                        (int)(Double.valueOf(next.getSpareSpace()) / next.getCapacity()
                                - Double.valueOf(current.getSpareSpace()) / current.getCapacity()) * maximum)
                .findFirst()
                .map(lot->lot.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }
}
