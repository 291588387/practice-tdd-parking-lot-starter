package com.parkinglot;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ParkingLot {
    private int capacity = 10;
    private final Map<ParkingTicket, Car> ticketCarMap = new HashMap<>();

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public ParkingTicket park(Car car) {
        if (ticketCarMap.size() >= capacity) throw new NoAvailablePositionException();
        ParkingTicket ticket = new ParkingTicket();
        ticketCarMap.put(ticket, car);
        return ticket;
    }

    public Car fetch(ParkingTicket parkingTicket) {
        if (!ticketCarMap.containsKey(parkingTicket)) {
            throw new UnrecognizedParkingTicketException();
        }
        Car car = ticketCarMap.get(parkingTicket);
        ticketCarMap.remove(parkingTicket);
        return car;
    }

    public boolean isFull() {
        return this.capacity <= this.ticketCarMap.size();
    }

    public boolean isContains(ParkingTicket ticket) {
        return ticketCarMap.containsKey(ticket);
    }

    public int getSpareSpace() {
        return this.capacity - this.ticketCarMap.size();
    }
}
