package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        //when
        ParkingTicket ticket = parkingLot.park(car);
        //then
        assertNotNull(ticket);
    }
    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);
        //when
        Car fetchedCar = parkingLot.fetch(parkingTicket);
        //then
        assertEquals(car, fetchedCar);
    }
    
    @Test
    void should_return_2_right_cars_when_fetch_given_parking_lot_and_2_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket ticket1 = parkingLot.park(car1);
        ParkingTicket ticket2 = parkingLot.park(car2);
        //when
        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);
        //then
        assertEquals(fetchedCar1, car1);
        assertEquals(fetchedCar2, car2);
    }

    @Test
    void should_throw_unrecognized_ticket_exception_when_fetch_given_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Car car2 = new Car();
        parkingLot.park(car1);
        parkingLot.park(car2);
        //when
        //then
        assertThrows(UnrecognizedParkingTicketException.class, () -> {parkingLot.fetch(new ParkingTicket());}, "Unrecognized Parking Ticket");
    }

    @Test
    void should_throw_unrecognized_ticket_exception_when_fetch_given_parking_lot_and_used_ticked() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        ParkingTicket ticket1 = parkingLot.park(car1);
        parkingLot.fetch(ticket1);
        //when
        //then
        assertThrows(UnrecognizedParkingTicketException.class, () -> {parkingLot.fetch(ticket1);}, "Unrecognized Parking Ticket");
    }

    @Test
    void should_throw_no_available_position_exception_when_park_given_full_parking_lot() {
        //given
        ParkingLot parkingLot = new ParkingLot(3);
        for (int i = 0; i < parkingLot.getCapacity() - 1; i++) {
            parkingLot.park(new Car());
        }
        parkingLot.park(new Car());
        //when
        //then
        assertThrows(NoAvailablePositionException.class, () -> {parkingLot.park(new Car());}, "No Available Position");
    }
}
