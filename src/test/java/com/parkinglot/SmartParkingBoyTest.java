package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SmartParkingBoyTest {
    @Test
    void should_return_a_car_when_fetch_given_parking_lot_and_ticket_and_smart_boy() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.addParkingLot(parkingLot);
        ParkingTicket ticket = parkingBoy.parkCar(car);
        // when
        Car fetchCar = parkingBoy.fetch(ticket);
        // then
        assertEquals(car, fetchCar);

    }

    @Test
    void should_return_ticket_of_parking_lot_2_when_park_given_boy_and_two_parking_lot_and_a_car() {
        // given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(4);
        smartParkingBoy.addParkingLot(parkingLot1);
        smartParkingBoy.addParkingLot(parkingLot2);
        Car car = new Car();
        // when
        ParkingTicket ticket = smartParkingBoy.parkCar(car);
        // then
        assertNotNull(ticket);
        assertTrue(parkingLot2.isContains(ticket));
    }

    @Test
    void should_throw_unrecognized_when_fetch_given_parking_lot_and_used_ticket_and_boy() {
        // used ticket
        //given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);
        ParkingTicket ticket = parkingBoy.parkCar(car);
        parkingBoy.fetch(ticket);

        // when
        // then
        UnrecognizedParkingTicketException unrecognizedTicketException = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.fetch(ticket));
        assertEquals("Unrecognized Parking Ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_unrecognized_when_fetch_given_parking_lot_and_wrong_ticket_and_smart_boy() {
        //given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);
        parkingBoy.parkCar(car);
        ParkingTicket wrongTicket = new ParkingTicket();

        // when
        // then
        UnrecognizedParkingTicketException unrecognizedTicketException = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.fetch(wrongTicket));
        assertEquals("Unrecognized Parking Ticket", unrecognizedTicketException.getMessage());
    }


    @Test
    void should_throw_no_available_when_fetch_given_no_position_parking_lot_and_car_and_boy() {
        //given
        Car car11 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);
        for (int i = 0; i < parkingLot1.getCapacity() * 2; i++) {
            parkingBoy.parkCar(new Car());
        }
        // when
        // then
        NoAvailablePositionException noAvailablePositionException = assertThrows(NoAvailablePositionException.class, () -> parkingBoy.parkCar(car11));
        assertEquals("No Available Position", noAvailablePositionException.getMessage());
    }

    @Test
    void should_return_two_car_when_fetch_given_parking_lot_and_two_ticket_and_smart_boy() {
        //given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);
        ParkingTicket ticket1 = parkingBoy.parkCar(car1);
        ParkingTicket ticket2 = parkingBoy.parkCar(car2);
        // when
        Car fetchCar1 = parkingBoy.fetch(ticket1);
        Car fetchCar2 = parkingBoy.fetch(ticket2);
        // then
        assertEquals(car1, fetchCar1);
        assertEquals(car2, fetchCar2);
    }

    @Test
    void should_return_a_ticket_when_fetch_given_two_parking_lot_and_car_and_smart_boy() {
        //given
        Car car11 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);
        for (int i = 0; i < parkingLot1.getCapacity(); i++) {
            parkingLot1.park(new Car());
        }
        // when
        ParkingTicket ticket = parkingBoy.parkCar(car11);
        // then
        assertNotNull(ticket);
    }

}
